package models;

import com.kornelkmiecik.checkoutsystem.model.*;
import com.kornelkmiecik.checkoutsystem.model.promotion.BuyXGetYFreePromotion;
import com.kornelkmiecik.checkoutsystem.model.promotion.FlatPercentPromotion;
import com.kornelkmiecik.checkoutsystem.model.promotion.QtyBasedPromotion;
import org.junit.Before;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Currency;
import java.util.Locale;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ReceiptPrintTest {
    private final PrintStream standardOut = System.out;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    @BeforeEach
    public void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));
    }

    @Before
    public void initStaticSupermarket() {
        new Supermarket("Food Shop", asList());
    }

    @Test
    public void totalBasePricePrintedProperly() {
        QtyBasedPromotion qtyBasedPromotion = new QtyBasedPromotion("promotionID1", 1, 4000);
        BuyXGetYFreePromotion buyXGetYFreePromotion = new BuyXGetYFreePromotion("promotionID2", 2, 1);
        FlatPercentPromotion flatPercentPromotion = new FlatPercentPromotion("promotionID3", 10);

        Product productA = new Product("productID1", "Pizza", 500, asList(qtyBasedPromotion, flatPercentPromotion, buyXGetYFreePromotion));
        Product productB = new Product("productID2", "Burger", 500, asList(qtyBasedPromotion, flatPercentPromotion, buyXGetYFreePromotion));

        Receipt receipt = new Receipt(new ArrayList<Product>(Arrays.asList(productA, productB)), Currency.getInstance(Locale.UK));
        assertEquals(0, new BigDecimal("10").compareTo(receipt.getTotalBasePriceInBigDecimals()));
        receipt.printBasePriceTotal();
        assertEquals("TOTAL BASE PRICE: " + new BigDecimal("10")  + " " + Currency.getInstance(Locale.UK).getSymbol(), outputStreamCaptor.toString().trim());
    }

    @Test
    public void totalPriceToPayPrintedProperly() {
        QtyBasedPromotion qtyBasedPromotion = new QtyBasedPromotion("promotionID1", 1, 4000);
        BuyXGetYFreePromotion buyXGetYFreePromotion = new BuyXGetYFreePromotion("promotionID2", 2, 1);
        FlatPercentPromotion flatPercentPromotion = new FlatPercentPromotion("promotionID3", 10);

        Product productA = new Product("productID1", "Pizza", 500, asList(qtyBasedPromotion, flatPercentPromotion, buyXGetYFreePromotion));
        Product productB = new Product("productID2", "Burger", 500, asList(qtyBasedPromotion, flatPercentPromotion, buyXGetYFreePromotion));

        Receipt receipt = new Receipt(new ArrayList<Product>(Arrays.asList(productA, productB)), Currency.getInstance(Locale.UK));
        assertEquals(0, new BigDecimal("10").compareTo(receipt.getTotalBasePriceInBigDecimals()));
        receipt.printTotalToPay();
        assertEquals("TOTAL PRICE TO PAY: " + new BigDecimal("10") + " " + Currency.getInstance(Locale.UK).getSymbol(), outputStreamCaptor.toString().trim());
    }

    @Test
    public void totalDeductionsPrintedProperly() {
        QtyBasedPromotion qtyBasedPromotion = new QtyBasedPromotion("promotionID1", 1, 400);
        BuyXGetYFreePromotion buyXGetYFreePromotion = new BuyXGetYFreePromotion("promotionID2", 2, 1);

        Basket basket = new Basket();
        Product productA = new Product("productID1", "Pizza", 800, asList(buyXGetYFreePromotion, qtyBasedPromotion));
        basket.addProduct(productA);

        Supermarket.promotionsInSupermarket = asList(buyXGetYFreePromotion, qtyBasedPromotion);
        Checkout checkout = new Checkout(Currency.getInstance(Locale.UK));
        ArrayList<Product> productListAfterAllPromotionsApplied = checkout.getProductListAfterAllPromotionsApplied(basket);

        BigDecimal productsPriceToPayTotal = productListAfterAllPromotionsApplied.stream()
                .map(product -> product.getPriceToPay())
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        BigDecimal predictedProductsTotal = new BigDecimal("4");
        assertEquals(0, predictedProductsTotal.compareTo(productsPriceToPayTotal));

        Receipt receipt = new Receipt(productListAfterAllPromotionsApplied, Currency.getInstance(Locale.UK));

        assertEquals(0,  new BigDecimal("4").compareTo(receipt.getTotalDeductions()));
        receipt.printTotalDeductions();
        assertEquals("TOTAL DEDUCTIONS: " + new BigDecimal("4")  + " " + Currency.getInstance(Locale.UK).getSymbol(), outputStreamCaptor.toString().trim());
    }
}
