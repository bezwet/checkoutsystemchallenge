package models;


import com.kornelkmiecik.checkoutsystem.model.*;
import com.kornelkmiecik.checkoutsystem.model.promotion.BuyXGetYFreePromotion;
import com.kornelkmiecik.checkoutsystem.model.promotion.FlatPercentPromotion;
import com.kornelkmiecik.checkoutsystem.model.promotion.QtyBasedPromotion;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Currency;
import java.util.Locale;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ReceiptTest {

    @Before
    public void initStaticSupermarket() {
        new Supermarket("Food Shop", asList());
    }

    @Test
    public void nullPointerIfReceiptProductListIsNull() {
        assertThatThrownBy(() -> new Receipt(null, Currency.getInstance(Locale.UK))).isInstanceOf(NullPointerException.class);
    }

    @Test
    public void totalPriceToPayCalculatedProperly() {
        QtyBasedPromotion qtyBasedPromotion = new QtyBasedPromotion("promotionID1", 1, 4000);
        BuyXGetYFreePromotion buyXGetYFreePromotion = new BuyXGetYFreePromotion("promotionID2", 2, 1);
        FlatPercentPromotion flatPercentPromotion = new FlatPercentPromotion("promotionID3", 10);

        Product productA = new Product("productID1", "Pizza", 500, asList(qtyBasedPromotion, flatPercentPromotion,buyXGetYFreePromotion));
        Product productB = new Product("productID2", "Burger", 500, asList(qtyBasedPromotion, flatPercentPromotion, buyXGetYFreePromotion));

        Receipt receipt = new Receipt(new ArrayList<Product>(Arrays.asList(productA, productB)), Currency.getInstance(Locale.UK));
        assertEquals(0,  new BigDecimal("10").compareTo(receipt.getTotalPriceToPay()));
    }

    @Test
    public void totalBasePriceCalculatedProperly() {
        QtyBasedPromotion qtyBasedPromotion = new QtyBasedPromotion("promotionID1", 1, 4000);
        BuyXGetYFreePromotion buyXGetYFreePromotion = new BuyXGetYFreePromotion("promotionID2", 2, 1);
        FlatPercentPromotion flatPercentPromotion = new FlatPercentPromotion("promotionID3", 10);

        Product productA = new Product("productID1", "Pizza", 500, asList(qtyBasedPromotion, flatPercentPromotion,buyXGetYFreePromotion));
        Product productB = new Product("productID2", "Burger", 500, asList(qtyBasedPromotion, flatPercentPromotion, buyXGetYFreePromotion));

        Receipt receipt = new Receipt(new ArrayList<Product>(Arrays.asList(productA, productB)), Currency.getInstance(Locale.UK));
        assertEquals(0,  new BigDecimal("10").compareTo(receipt.getTotalBasePriceInBigDecimals()));
    }

    @Test
    public void totalDeductionsCalculatedProperly() {
        QtyBasedPromotion qtyBasedPromotion = new QtyBasedPromotion("promotionID1", 1, 400);
        BuyXGetYFreePromotion buyXGetYFreePromotion = new BuyXGetYFreePromotion("promotionID2", 2, 1);

        Basket basket = new Basket();
        Product productA = new Product("productID1", "Pizza", 800, asList(buyXGetYFreePromotion, qtyBasedPromotion));
        basket.addProduct(productA);

        Supermarket.promotionsInSupermarket = asList(buyXGetYFreePromotion, qtyBasedPromotion);
        Checkout checkout = new Checkout(Currency.getInstance(Locale.UK));
        ArrayList<Product> productListAfterAllPromotionsApplied = checkout.getProductListAfterAllPromotionsApplied(basket);

        BigDecimal productsPriceToPayTotal = productListAfterAllPromotionsApplied.stream()
                .map(product -> product.getPriceToPay())
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        BigDecimal predictedProductsTotal = new BigDecimal("4");

        assertEquals(0, predictedProductsTotal.compareTo(productsPriceToPayTotal));

        Receipt receipt = new Receipt(productListAfterAllPromotionsApplied, Currency.getInstance(Locale.UK));

        assertEquals(0,  new BigDecimal("4").compareTo(receipt.getTotalDeductions()));
    }
}
