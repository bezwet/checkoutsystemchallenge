package models;

import com.kornelkmiecik.checkoutsystem.model.Basket;
import com.kornelkmiecik.checkoutsystem.model.Product;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class BasketTest {

    @Test
    public void canProductBeenAddedToBasket() {
        Basket basket = new Basket();
        Product product = new Product("testID1234", "Pizza", 1);
        basket.addProduct(product);
        assertTrue(basket.getProductList().contains(product));
    }

    @Test
    public void nullPointerIfNullIsAddedToBasket() {
        Basket basket = new Basket();
        assertThatThrownBy(() -> basket.addProduct(null)).isInstanceOf(NullPointerException.class);
    }
    @Test
    public void canManyProductsOfDifferentTypeBeenAddedToBasket() {
        Basket basket = new Basket();
        Product productA = new Product("testID1234", "Pizza", 1);
        Product productB = new Product("testID1234", "Burger", 1);
        basket.addProduct(productA);
        basket.addProduct(productB);
        assertTrue(basket.getProductList().contains(productA));
        assertTrue(basket.getProductList().contains(productB));
    }

    @Test
    public void canManyProductsOfSameTypeBeenAddedToBasket() {
        Basket basket = new Basket();
        Product productA = new Product("testID1234", "Pizza", 1);
        Product productB = new Product("testID1234", "Pizza", 1);
        basket.addProduct(productA);
        basket.addProduct(productB);
        assertTrue(basket.getProductList().contains(productA));
        assertTrue(basket.getProductList().contains(productB));
    }


}
