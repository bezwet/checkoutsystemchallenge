package models.promotion;

import com.kornelkmiecik.checkoutsystem.model.enums.PromotionType;
import com.kornelkmiecik.checkoutsystem.model.promotion.BasePromotion;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.assertj.core.api.Assertions.assertThatThrownBy;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class BasePromotionTest {
    @Test
    public void nullPointerIfTypeIsNull() {
        assertThatThrownBy(() -> new BasePromotion("testID1234", null)).isInstanceOf(NullPointerException.class);
    }

    @Test
    public void nullPointerIfIDIsNull() {
        assertThatThrownBy(() -> new BasePromotion(null, PromotionType.QTY_BASED_PRICE_OVERRIDE)).isInstanceOf(NullPointerException.class);
    }
}
