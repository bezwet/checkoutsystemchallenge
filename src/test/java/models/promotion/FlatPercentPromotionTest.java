package models.promotion;

import com.kornelkmiecik.checkoutsystem.model.Product;
import com.kornelkmiecik.checkoutsystem.model.enums.PromotionType;
import com.kornelkmiecik.checkoutsystem.model.promotion.BuyXGetYFreePromotion;
import com.kornelkmiecik.checkoutsystem.model.promotion.FlatPercentPromotion;
import com.kornelkmiecik.checkoutsystem.model.promotion.QtyBasedPromotion;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class FlatPercentPromotionTest {

    @Test
    public void promotionTypeCorrect() {
        FlatPercentPromotion flatPercentPromotion = new FlatPercentPromotion("testID1234", 10);
        assertEquals(PromotionType.FLAT_PERCENT, flatPercentPromotion.getType());
    }

    @Test
    public void nullPointerIfIDIsNull() {
        assertThatThrownBy(() -> new FlatPercentPromotion(null, 1)).isInstanceOf(NullPointerException.class);
    }

    @Test
    public void illegalArgumentIfAmountIsLessThanZero() {
        assertThatThrownBy(() -> new FlatPercentPromotion("testID1234", -1)).isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    public void correctCountOfProductsThatApplyForPromotion() {
        FlatPercentPromotion flatPercentPromotion = new FlatPercentPromotion("testID1234", 10);
        Product productA = new Product("testProductID1234", "Pizza", 12, asList(flatPercentPromotion));
        Product productB = new Product("testProductID1234", "Pizza", 12, asList(flatPercentPromotion));
        Product productC = new Product("testProductID2345", "Burger", 12);
        Collection<Product> products = asList(productA, productB, productC);
        assertEquals(2, flatPercentPromotion.countProductsThatSharePromotion(products));
    }

    @Test
    public void incorrectCountOfProductsThatApplyForPromotion() {
        FlatPercentPromotion flatPercentPromotion = new FlatPercentPromotion("testID1234", 10);
        Product productA = new Product("testProductID1234", "Pizza", 12, asList(flatPercentPromotion));
        Product productB = new Product("testProductID1234", "Pizza", 12, asList(flatPercentPromotion));
        Product productC = new Product("testProductID2345", "Burger", 12);
        Collection<Product> products = asList(productA, productB, productC);
        assertNotEquals(3, flatPercentPromotion.countProductsThatSharePromotion(products));
    }

    @Test
    public void isProductsTotalPriceCorrectAfterPromotionApplied() {
        FlatPercentPromotion flatPercentPromotion = new FlatPercentPromotion("testID1234", 10);
        Product productA = new Product("testProductID1234", "Pizza", 4000, asList(flatPercentPromotion));
        Product productB = new Product("testProductID1234", "Pizza", 4000, asList(flatPercentPromotion));
        Product productC = new Product("testProductID2345", "Burger", 2000);

        BigDecimal predictedProductsTotal = new BigDecimal("92");

        ArrayList<Product> products = new ArrayList<Product>(Arrays.asList(productA, productB, productC));
        ArrayList<Product> productsAfterPromotionApplied = flatPercentPromotion.getProductsAfterPromotionApplied(products);
        BigDecimal productsPriceToPayTotal = productsAfterPromotionApplied.stream()
                .map(product -> product.getPriceToPay())
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        assertEquals(0, predictedProductsTotal.compareTo(productsPriceToPayTotal));
    }
}