package models.promotion;

import com.kornelkmiecik.checkoutsystem.Utils;
import com.kornelkmiecik.checkoutsystem.model.Product;
import com.kornelkmiecik.checkoutsystem.model.enums.PromotionType;
import com.kornelkmiecik.checkoutsystem.model.promotion.BuyXGetYFreePromotion;
import com.kornelkmiecik.checkoutsystem.model.promotion.QtyBasedPromotion;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Stream;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class BuyXGetYFreePromotionTest {

    @Test
    public void promotionTypeCorrect() {
        BuyXGetYFreePromotion buyXGetYFreePromotion = new BuyXGetYFreePromotion("testID1234", 10, 4);
        assertEquals(PromotionType.BUY_X_GET_Y_FREE, buyXGetYFreePromotion.getType());
    }

    @Test
    public void nullPointerIfIDIsNull() {
        assertThatThrownBy(() -> new BuyXGetYFreePromotion(null, 1, 4)).isInstanceOf(NullPointerException.class);
    }

    @Test
    public void illegalArgumentIfReqQtyIsLessThanZero() {
        assertThatThrownBy(() -> new BuyXGetYFreePromotion("testID1234", -1, 4)).isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    public void illegalArgumentIfFreeQtyIsLessThanZero() {
        assertThatThrownBy(() -> new BuyXGetYFreePromotion("testID1234", 1, -10)).isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    public void illegalArgumentIfReqQtyIsLessThanFreeQty() {
        assertThatThrownBy(() -> new BuyXGetYFreePromotion("testID1234", 1, 2)).isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    public void illegalArgumentIfReqQtyIsEqualFreeQty() {
        assertThatThrownBy(() -> new BuyXGetYFreePromotion("testID1234", 1, 1)).isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    public void correctCountOfProductsThatApplyForPromotion() {
        BuyXGetYFreePromotion buyXGetYFreePromotion = new BuyXGetYFreePromotion("testID1234", 7, 4);
        Product productA = new Product("testProductID1234", "Pizza", 12, asList(buyXGetYFreePromotion));
        Product productB = new Product("testProductID1234", "Pizza", 12, asList(buyXGetYFreePromotion));
        Product productC = new Product("testProductID2345", "Burger", 12);
        Collection<Product> products = asList(productA, productB, productC);
        assertEquals(2, buyXGetYFreePromotion.countProductsThatSharePromotion(products));
    }

    @Test
    public void incorrectCountOfProductsThatApplyForPromotion() {
        BuyXGetYFreePromotion buyXGetYFreePromotion = new BuyXGetYFreePromotion("testID1234", 10, 4);
        Product productA = new Product("testProductID1234", "Pizza", 12, asList(buyXGetYFreePromotion));
        Product productB = new Product("testProductID1234", "Pizza", 12, asList(buyXGetYFreePromotion));
        Product productC = new Product("testProductID2345", "Burger", 12);
        Collection<Product> products = asList(productA, productB, productC);
        assertNotEquals(3, buyXGetYFreePromotion.countProductsThatSharePromotion(products));
    }

    @Test
    public void productIsAllowedForPromotion() {
        BuyXGetYFreePromotion buyXGetYFreePromotion = new BuyXGetYFreePromotion("testID1234", 2, 1);
        Product productA = new Product("testProductID1234", "Pizza", 12, asList(buyXGetYFreePromotion));
        Product productB = new Product("testProductID1234", "Pizza", 12, asList(buyXGetYFreePromotion));
        Product productC = new Product("testProductID2345", "Burger", 12);
        Collection<Product> products = asList(productA, productB, productC);
        assertTrue(buyXGetYFreePromotion.requirementForPromotionMet(products));
    }

    @Test
    public void productIsNotAllowedForPromotion() {
        BuyXGetYFreePromotion buyXGetYFreePromotion = new BuyXGetYFreePromotion("testID1234", 5, 1);
        Product productA = new Product("testProductID1234", "Pizza", 12, asList(buyXGetYFreePromotion));
        Product productB = new Product("testProductID1234", "Pizza", 12, asList(buyXGetYFreePromotion));
        Product productC = new Product("testProductID2345", "Burger", 12);
        Collection<Product> products = asList(productA, productB, productC);
        assertFalse(buyXGetYFreePromotion.requirementForPromotionMet(products));
    }


    @Test
    public void isProductPriceCorrect() {
        BuyXGetYFreePromotion buyXGetYFreePromotion = new BuyXGetYFreePromotion("testID1234", 5, 1);
        Product productA = new Product("testProductID1234", "Pizza", 12, asList(buyXGetYFreePromotion));
        Product productB = new Product("testProductID1234", "Pizza", 12, asList(buyXGetYFreePromotion));
        Product productC = new Product("testProductID2345", "Burger", 12);
        Collection<Product> products = asList(productA, productB, productC);
        assertFalse(buyXGetYFreePromotion.requirementForPromotionMet(products));
    }

    @Test
    public void isProductsTotalPriceCorrectAfterPromotionApplied() {
        BuyXGetYFreePromotion buyXGetYFreePromotion = new BuyXGetYFreePromotion("testID1234", 2, 1);
        Product productA = new Product("testProductID1234", "Pizza", 500, asList(buyXGetYFreePromotion));
        Product productB = new Product("testProductID1234", "Pizza", 500, asList(buyXGetYFreePromotion));
        Product productC = new Product("testProductID2345", "Burger", 300);
        BigDecimal predictedProductsTotal = new BigDecimal("8");

        ArrayList<Product> products = new ArrayList<Product>(Arrays.asList(productA, productB, productC));
        ArrayList<Product> productsAfterPromotionApplied = buyXGetYFreePromotion.getProductsAfterPromotionApplied(products);
        BigDecimal productsPriceToPayTotal = productsAfterPromotionApplied.stream()
                .map(product -> product.getPriceToPay())
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        assertEquals(0, predictedProductsTotal.compareTo(productsPriceToPayTotal));

    }

    @Test
    public void isProductsTotalPriceCorrectAfterPromotionCheckedAndApplied() {
        BuyXGetYFreePromotion buyXGetYFreePromotion = new BuyXGetYFreePromotion("testID1234", 2, 1);
        Product productA = new Product("testProductID1234", "Pizza", 500, asList(buyXGetYFreePromotion));
        Product productB = new Product("testProductID1234", "Pizza", 500, asList(buyXGetYFreePromotion));
        Product productC = new Product("testProductID2345", "Burger", 300);
        BigDecimal predictedProductsTotal = new BigDecimal("8");

        ArrayList<Product> products = new ArrayList<Product>(Arrays.asList(productA, productB, productC));
        ArrayList<Product> productsAfterPromotionApplied = buyXGetYFreePromotion.getProductsAfterPromotionCheckedAndApplied(products);
        BigDecimal productsPriceToPayTotal = productsAfterPromotionApplied.stream()
                .map(product -> product.getPriceToPay())
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        assertEquals(0, predictedProductsTotal.compareTo(productsPriceToPayTotal));
    }

}
