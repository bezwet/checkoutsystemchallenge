package models.promotion;

import com.kornelkmiecik.checkoutsystem.model.Product;
import com.kornelkmiecik.checkoutsystem.model.enums.PromotionType;
import com.kornelkmiecik.checkoutsystem.model.promotion.BuyXGetYFreePromotion;
import com.kornelkmiecik.checkoutsystem.model.promotion.FlatPercentPromotion;
import com.kornelkmiecik.checkoutsystem.model.promotion.QtyBasedPromotion;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.hamcrest.MatcherAssert.assertThat;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class QtyBasedPromotionTest {

    @Test
    public void promotionTypeCorrect() {
        QtyBasedPromotion qtyBasedPromotion = new QtyBasedPromotion("testID1234", 1, 4);
        assertEquals(PromotionType.QTY_BASED_PRICE_OVERRIDE, qtyBasedPromotion.getType());
    }

    @Test
    public void nullPointerIfIDIsNull() {
        assertThatThrownBy(() -> new QtyBasedPromotion(null, 1, 4)).isInstanceOf(NullPointerException.class);
    }

    @Test
    public void illegalArgumentIfReqQtyIsLessThanZero() {
        assertThatThrownBy(() -> new QtyBasedPromotion("testID1234", -1, 4)).isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    public void illegalArgumentIfPriceIsLessThanZero() {
        assertThatThrownBy(() -> new QtyBasedPromotion("testID1234", 1, -10)).isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    public void correctCountOfProductsThatApplyForPromotion() {
        QtyBasedPromotion qtyBasedPromotion = new QtyBasedPromotion("testID1234", 10, 4);
        Product productA = new Product("testProductID1234", "Pizza", 12, asList(qtyBasedPromotion));
        Product productB = new Product("testProductID1234", "Pizza", 12, asList(qtyBasedPromotion));
        Product productC = new Product("testProductID2345", "Burger", 12);
        Collection<Product> products = asList(productA, productB, productC);
        assertEquals(2, qtyBasedPromotion.countProductsThatSharePromotion(products));
    }

    @Test
    public void incorrectCountOfProductsThatApplyForPromotion() {
        QtyBasedPromotion qtyBasedPromotion = new QtyBasedPromotion("testID1234", 10, 4);
        Product productA = new Product("testProductID1234", "Pizza", 12, asList(qtyBasedPromotion));
        Product productB = new Product("testProductID1234", "Pizza", 12, asList(qtyBasedPromotion));
        Product productC = new Product("testProductID2345", "Burger", 12);
        Collection<Product> products = asList(productA, productB, productC);
        assertNotEquals(3, qtyBasedPromotion.countProductsThatSharePromotion(products));
    }

    @Test
    public void productIsAllowedForPromotion() {
        QtyBasedPromotion qtyBasedPromotion = new QtyBasedPromotion("testID1234", 2, 1);
        Product productA = new Product("testProductID1234", "Pizza", 12, asList(qtyBasedPromotion));
        Product productB = new Product("testProductID1234", "Pizza", 12, asList(qtyBasedPromotion));
        Product productC = new Product("testProductID2345", "Burger", 12);
        Collection<Product> products = asList(productA, productB, productC);
        assertTrue(qtyBasedPromotion.requirementForPromotionMet(products));
    }

    @Test
    public void productIsNotAllowedForPromotion() {
        QtyBasedPromotion qtyBasedPromotion = new QtyBasedPromotion("testID1234", 5, 1);
        Product productA = new Product("testProductID1234", "Pizza", 12, asList(qtyBasedPromotion));
        Product productB = new Product("testProductID1234", "Pizza", 12, asList(qtyBasedPromotion));
        Product productC = new Product("testProductID2345", "Burger", 12);
        Collection<Product> products = asList(productA, productB, productC);
        assertFalse(qtyBasedPromotion.requirementForPromotionMet(products));
    }

    @Test
    public void isProductsTotalPriceCorrectAfterPromotionApplied() {
        QtyBasedPromotion qtyBasedPromotion = new QtyBasedPromotion("testID1234", 1, 100);
        Product productA = new Product("testProductID1234", "Pizza", 120, asList(qtyBasedPromotion));
        Product productB = new Product("testProductID1234", "Pizza", 120, asList(qtyBasedPromotion));
        Product productD = new Product("testProductID1234", "Pizza", 120, asList(qtyBasedPromotion));
        ArrayList<Product> products = new ArrayList<Product>(Arrays.asList(productA, productB, productD));
        BigDecimal predictedProductsTotal = new BigDecimal("3");

        ArrayList<Product> productsAfterPromotionApplied = qtyBasedPromotion.getProductsAfterPromotionApplied(products);
        BigDecimal productsPriceToPayTotal = productsAfterPromotionApplied.stream()
                .map(product -> product.getPriceToPay())
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        assertEquals(0, predictedProductsTotal.compareTo(productsPriceToPayTotal));
    }

    @Test
    public void isProductsTotalPriceCorrectAfterPromotionCheckedAndApplied() {
        QtyBasedPromotion qtyBasedPromotion = new QtyBasedPromotion("testID1234", 1, 100);
        Product productA = new Product("testProductID1234", "Pizza", 120, asList(qtyBasedPromotion));
        Product productB = new Product("testProductID1234", "Pizza", 120, asList(qtyBasedPromotion));
        Product productD = new Product("testProductID1234", "Pizza", 120, asList(qtyBasedPromotion));
        ArrayList<Product> products = new ArrayList<Product>(Arrays.asList(productA, productB, productD));
        BigDecimal predictedProductsTotal = new BigDecimal("3");

        ArrayList<Product> productsAfterPromotionApplied = qtyBasedPromotion.getProductsAfterPromotionApplied(products);
        BigDecimal productsPriceToPayTotal = productsAfterPromotionApplied.stream()
                .map(product -> product.getPriceToPay())
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        assertEquals(0, predictedProductsTotal.compareTo(productsPriceToPayTotal));
    }
}
