package models;

import com.kornelkmiecik.checkoutsystem.model.Basket;
import com.kornelkmiecik.checkoutsystem.model.Product;
import com.kornelkmiecik.checkoutsystem.model.User;
import com.kornelkmiecik.checkoutsystem.model.promotion.BuyXGetYFreePromotion;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class UserTest {
    @Test
    public void nullPointerIfFirstnameIsNull() {
        assertThatThrownBy(() -> new User(null, "Smith", new BigDecimal("10"))).isInstanceOf(NullPointerException.class);
    }

    @Test
    public void nullPointerIfLastnameIsNull() {
        assertThatThrownBy(() -> new User("Adam", null, new BigDecimal("10"))).isInstanceOf(NullPointerException.class);
    }

    @Test
    public void nullPointerIfWalletIsNull() {
        assertThatThrownBy(() -> new User("Adam", "Smith", null)).isInstanceOf(NullPointerException.class);
    }

    @Test
    public void trueIfUserCanAddProductToBasket() {
        User user = new User("Adam", "Smith", new BigDecimal("100"));
        user.setBasket(new Basket());
        Product productB = new Product("testID1234", "Burger", 1);
        user.getBasket().addProduct(productB);
        assertTrue(user.getBasket().getProductList().contains(productB));
    }

    @Test
    public void trueIfUserCanAddManyProductsToBasket() {
       User user = new User("Adam", "Smith", new BigDecimal("100"));
        user.setBasket(new Basket());
        Product productA = new Product("testID1234", "Pizza", 1);
        Product productB = new Product("testID1234", "Burger", 1);
        user.getBasket().addProduct(productA);
        user.getBasket().addProduct(productB);
        assertTrue(user.getBasket().getProductList().contains(productA));
        assertTrue(user.getBasket().getProductList().contains(productB));
    }

    @Test
    public void trueIfUserPaid() {
        User user = new User("Adam", "Smith", new BigDecimal("100"));
        assertTrue(user.pay(new BigDecimal(30)));
    }

    @Test
    public void falseIfUserDidNotPaid() {
        User user = new User("Adam", "Smith", new BigDecimal("100"));
        assertFalse(user.pay(new BigDecimal(300)));
    }

    @Test
    public void trueIfCorrectAmountRemovedFromUserWallet() {
        User user = new User("Adam", "Smith", new BigDecimal("100"));
        user.pay(new BigDecimal(50));
        assertEquals(new BigDecimal("50"), user.getWalletAmountInPounds());
    }

    @Test
    public void falseIfIncorrectAmountRemovedFromUserWallet() {
        User user = new User("Adam", "Smith", new BigDecimal("100"));
        user.pay(new BigDecimal(50));
        assertNotEquals(new BigDecimal("10"), user.getWalletAmountInPounds());
    }

    @Test
    public void nullPointerExceptionIfPriceToPayIsNull() {
        User user = new User("Adam", "Smith", new BigDecimal("100"));
        assertThatThrownBy(() -> user.pay(null)).isInstanceOf(NullPointerException.class);
    }

    @Test
    public void illegalArgumentExceptionIfPriceToPayIsLessThanZero() {
        User user = new User("Adam", "Smith", new BigDecimal("100"));
        assertThatThrownBy(() -> user.pay(new BigDecimal(-10))).isInstanceOf(IllegalArgumentException.class);
    }

}
