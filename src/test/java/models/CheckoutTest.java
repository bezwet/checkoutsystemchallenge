package models;

import com.kornelkmiecik.checkoutsystem.model.Basket;
import com.kornelkmiecik.checkoutsystem.model.Checkout;
import com.kornelkmiecik.checkoutsystem.model.Product;
import com.kornelkmiecik.checkoutsystem.model.Supermarket;
import com.kornelkmiecik.checkoutsystem.model.promotion.BuyXGetYFreePromotion;
import com.kornelkmiecik.checkoutsystem.model.promotion.FlatPercentPromotion;
import com.kornelkmiecik.checkoutsystem.model.promotion.QtyBasedPromotion;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.test.web.client.MockRestServiceServer;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Currency;
import java.util.Locale;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class CheckoutTest {

@Before
public void initStaticSupermarket() {
    new Supermarket("Food Shop", asList());
}
    @Test
    public void promotionNotAppliedIfNotDefinedInSupermarket() {
        QtyBasedPromotion qtyBasedPromotion = new QtyBasedPromotion("promotionID1", 1, 400);
        BuyXGetYFreePromotion buyXGetYFreePromotion = new BuyXGetYFreePromotion("promotionID2", 2, 1);

        Basket basket = new Basket();
        Product productA = new Product("productID1", "Pizza", 800, asList(buyXGetYFreePromotion, qtyBasedPromotion));
        basket.addProduct(productA);

        Supermarket.promotionsInSupermarket = asList();

        Checkout checkout = new Checkout(Currency.getInstance(Locale.UK));
        ArrayList<Product> productListAfterAllPromotionsApplied = checkout.getProductListAfterAllPromotionsApplied(basket);

        BigDecimal productsPriceToPayTotal = productListAfterAllPromotionsApplied.stream()
                .map(product -> product.getPriceToPay())
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        BigDecimal predictedProductsTotal = new BigDecimal("8");

        assertEquals(0, predictedProductsTotal.compareTo(productsPriceToPayTotal));
    }


    @Test
    public void checkoutTotalToPayCorrectForOneProductsWith2DifferentPromotions() {
        QtyBasedPromotion qtyBasedPromotion = new QtyBasedPromotion("promotionID1", 1, 400);
        BuyXGetYFreePromotion buyXGetYFreePromotion = new BuyXGetYFreePromotion("promotionID2", 2, 1);

        Basket basket = new Basket();
        Product productA = new Product("productID1", "Pizza", 800, asList(buyXGetYFreePromotion, qtyBasedPromotion));
        basket.addProduct(productA);

        Checkout checkout = new Checkout(Currency.getInstance(Locale.UK));
        Supermarket.promotionsInSupermarket = asList(buyXGetYFreePromotion, qtyBasedPromotion);

        ArrayList<Product> productListAfterAllPromotionsApplied = checkout.getProductListAfterAllPromotionsApplied(basket);

        BigDecimal productsPriceToPayTotal = productListAfterAllPromotionsApplied.stream()
                .map(product -> product.getPriceToPay())
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        BigDecimal predictedProductsTotal = new BigDecimal("4");

        assertEquals(0, predictedProductsTotal.compareTo(productsPriceToPayTotal));
    }


    @Test
    public void checkoutTotalToPayCorrectFor2ProductsWith2DifferentPromotions() {
        QtyBasedPromotion qtyBasedPromotion = new QtyBasedPromotion("promotionID1", 1, 400);
        BuyXGetYFreePromotion buyXGetYFreePromotion = new BuyXGetYFreePromotion("promotionID2", 2, 1);

        Basket basket = new Basket();
        Product productA = new Product("productID1", "Pizza", 800, asList(buyXGetYFreePromotion));
        Product productB = new Product("productID2", "Burger", 500, asList(qtyBasedPromotion));
        basket.addProduct(productA);
        basket.addProduct(productB);

        Supermarket.promotionsInSupermarket = asList(buyXGetYFreePromotion, qtyBasedPromotion);
        Checkout checkout = new Checkout(Currency.getInstance(Locale.UK));
        ArrayList<Product> productListAfterAllPromotionsApplied = checkout.getProductListAfterAllPromotionsApplied(basket);

        BigDecimal productsPriceToPayTotal = productListAfterAllPromotionsApplied.stream()
                .map(product -> product.getPriceToPay())
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        BigDecimal predictedProductsTotal = new BigDecimal("12");

        assertEquals(0, predictedProductsTotal.compareTo(productsPriceToPayTotal));
    }

    @Test
    public void checkoutTotalToPayCorrectFor3ProductsWith3DifferentPromotions() {
        QtyBasedPromotion qtyBasedPromotion = new QtyBasedPromotion("promotionID1", 1, 400);
        BuyXGetYFreePromotion buyXGetYFreePromotion = new BuyXGetYFreePromotion("promotionID2", 2, 1);
        FlatPercentPromotion flatPercentPromotion = new FlatPercentPromotion("promotionID3", 10);

        Basket basket = new Basket();
        Product productA = new Product("productID1", "Pizza", 500, asList(buyXGetYFreePromotion));
        Product productB = new Product("productID2", "Burger", 500, asList(qtyBasedPromotion));
        Product productC = new Product("productID3", "Fries", 1000, asList(flatPercentPromotion));
        basket.addProduct(productA);
        basket.addProduct(productB);
        basket.addProduct(productC);

        Supermarket.promotionsInSupermarket = asList(buyXGetYFreePromotion, qtyBasedPromotion, flatPercentPromotion);
        Checkout checkout = new Checkout(Currency.getInstance(Locale.UK));
        ArrayList<Product> productListAfterAllPromotionsApplied = checkout.getProductListAfterAllPromotionsApplied(basket);

        BigDecimal productsPriceToPayTotal = productListAfterAllPromotionsApplied.stream()
                .map(product -> product.getPriceToPay())
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        BigDecimal predictedProductsTotal = new BigDecimal("18");

        assertEquals(0, predictedProductsTotal.compareTo(productsPriceToPayTotal));
    }

    @Test
    public void checkoutTotalToPayCorrectFor1ProductsWith3DifferentPromotions() {
        QtyBasedPromotion qtyBasedPromotion = new QtyBasedPromotion("promotionID1", 1, 4000);
        BuyXGetYFreePromotion buyXGetYFreePromotion = new BuyXGetYFreePromotion("promotionID2", 2, 1);
        FlatPercentPromotion flatPercentPromotion = new FlatPercentPromotion("promotionID3", 10);

        Basket basket = new Basket();
        Product productA = new Product("productID1", "Pizza", 5000, asList(buyXGetYFreePromotion, qtyBasedPromotion, flatPercentPromotion));
        basket.addProduct(productA);

        Supermarket.promotionsInSupermarket = asList(buyXGetYFreePromotion, qtyBasedPromotion, flatPercentPromotion);
        Checkout checkout = new Checkout(Currency.getInstance(Locale.UK));
        ArrayList<Product> productListAfterAllPromotionsApplied = checkout.getProductListAfterAllPromotionsApplied(basket);

        BigDecimal productsPriceToPayTotal = productListAfterAllPromotionsApplied.stream()
                .map(product -> product.getPriceToPay())
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        BigDecimal predictedProductsTotal = new BigDecimal("36");

        assertEquals(0, predictedProductsTotal.compareTo(productsPriceToPayTotal));
    }

    @Test
    public void checkoutTotalToPayCorrectFor2ProductsWith2SamePromotions() {
        QtyBasedPromotion qtyBasedPromotion = new QtyBasedPromotion("promotionID1", 1, 4000);
        BuyXGetYFreePromotion buyXGetYFreePromotion = new BuyXGetYFreePromotion("promotionID2", 2, 1);
        FlatPercentPromotion flatPercentPromotion = new FlatPercentPromotion("promotionID3", 10);

        Basket basket = new Basket();
        Product productA = new Product("productID1", "Pizza", 5000, asList(qtyBasedPromotion, flatPercentPromotion));
        Product productB = new Product("productID2", "Burger", 5000, asList(qtyBasedPromotion, flatPercentPromotion));
        basket.addProduct(productA);
        basket.addProduct(productB);

        Supermarket.promotionsInSupermarket = asList(buyXGetYFreePromotion, qtyBasedPromotion, flatPercentPromotion);
        Checkout checkout = new Checkout(Currency.getInstance(Locale.UK));
        ArrayList<Product> productListAfterAllPromotionsApplied = checkout.getProductListAfterAllPromotionsApplied(basket);

        BigDecimal productsPriceToPayTotal = productListAfterAllPromotionsApplied.stream()
                .map(product -> product.getPriceToPay())
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        BigDecimal predictedProductsTotal = new BigDecimal("72");

        assertEquals(0, predictedProductsTotal.compareTo(productsPriceToPayTotal));
    }

    @Test
    public void checkoutTotalToPayCorrectFor2ProductsWith3SamePromotions() {
        QtyBasedPromotion qtyBasedPromotion = new QtyBasedPromotion("promotionID1", 1, 4000);
        BuyXGetYFreePromotion buyXGetYFreePromotion = new BuyXGetYFreePromotion("promotionID2", 2, 1);
        FlatPercentPromotion flatPercentPromotion = new FlatPercentPromotion("promotionID3", 10);

        Basket basket = new Basket();
        Product productA = new Product("productID1", "Pizza", 5000, asList(qtyBasedPromotion, flatPercentPromotion, buyXGetYFreePromotion));
        Product productB = new Product("productID2", "Burger", 5000, asList(qtyBasedPromotion, flatPercentPromotion, buyXGetYFreePromotion));
        basket.addProduct(productA);
        basket.addProduct(productB);

        Supermarket.promotionsInSupermarket = asList(qtyBasedPromotion, flatPercentPromotion, buyXGetYFreePromotion);
        Checkout checkout = new Checkout(Currency.getInstance(Locale.UK));
        ArrayList<Product> productListAfterAllPromotionsApplied = checkout.getProductListAfterAllPromotionsApplied(basket);

        BigDecimal productsPriceToPayTotal = productListAfterAllPromotionsApplied.stream()
                .map(product -> product.getPriceToPay())
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        BigDecimal predictedProductsTotal = new BigDecimal("36");

        assertEquals(0, predictedProductsTotal.compareTo(productsPriceToPayTotal));
    }


}
