package models;

import com.kornelkmiecik.checkoutsystem.model.Product;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.assertj.core.api.Assertions.assertThatThrownBy;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ProductTest {

    @Test
    public void nullPointerIfIDIsNull() {
        assertThatThrownBy(() -> new Product(null, "Pizza", 10)).isInstanceOf(NullPointerException.class);
    }
    @Test
    public void nullPointerIfNameIsNull() {
        assertThatThrownBy(() -> new Product("testID1234", null, 1)).isInstanceOf(NullPointerException.class);
    }
}
