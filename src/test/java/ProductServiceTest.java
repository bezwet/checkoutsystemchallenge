import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kornelkmiecik.checkoutsystem.config.AppConfig;
import com.kornelkmiecik.checkoutsystem.model.Product;
import com.kornelkmiecik.checkoutsystem.services.api.ProductService;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = AppConfig.class)
public class ProductServiceTest {

    @Autowired
    private ProductService productService;

    @Test
    public void canGetSingleProduct() throws JsonProcessingException {
        Product product = new Product("PWWe3w1SDU", "Amazing Burger!", 999);
        Product returnedProduct = productService.getProduct(product.getId());
        Assert.assertEquals(product.getId(), returnedProduct.getId());
    }

    @Test
    public void canGetManyProducts() {
        Product[] returnedProducts = productService.getAllProducts();
        Assert.assertTrue(returnedProducts.length > 1);
    }

}
