import com.kornelkmiecik.checkoutsystem.Utils;
import com.kornelkmiecik.checkoutsystem.model.promotion.BuyXGetYFreePromotion;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class UtilsTest {

    @Test
    public void arePenniesProperlyConvertedToPounds() {
        BigDecimal pounds = new BigDecimal(10);
        assertEquals(0, pounds.compareTo(Utils.getPenniesToBigDecimalPound(1000)));
    }
    @Test
    public void isZeroPenniesProperlyConvertedToPounds() {
        BigDecimal pounds = new BigDecimal(0);
        assertEquals(0, pounds.compareTo(Utils.getPenniesToBigDecimalPound(0)));
    }

    @Test
    public void isLargePenniesNumberProperlyConvertedToPounds() {
        BigDecimal pounds = new BigDecimal(5000);
        assertEquals(0, pounds.compareTo(Utils.getPenniesToBigDecimalPound(500000)));
    }

    @Test
    public void isMinusPenniesNumberProperlyConvertedToPounds() {
        BigDecimal pounds = new BigDecimal(-10);
        assertEquals(0, pounds.compareTo(Utils.getPenniesToBigDecimalPound(-1000)));
    }

    @Test
    public void arePenniesProperlyConvertedToPoundWithRest() {
        BigDecimal pounds = new BigDecimal("10.01");
        assertEquals(0, pounds.compareTo(Utils.getPenniesToBigDecimalPound(1001)));
    }

    @Test
    public void isPercentFromBigDecimalCorrectlyCalculated() {
        BigDecimal pounds = new BigDecimal("100");
        assertEquals(0, pounds.compareTo(Utils.getPercentOutOfDecimal(new BigDecimal("1000"), 10)));
    }

    @Test
    public void IsPercentOfValueCorrectIfPercentIsZero() {
        BigDecimal pounds = new BigDecimal("0");
        assertEquals(0, pounds.compareTo(Utils.getPercentOutOfDecimal(new BigDecimal("100"), 0)));
    }

    @Test
    public void illegalArgumentIfPercentLessThanZero() {
        assertThatThrownBy(() -> Utils.getPercentOutOfDecimal(new BigDecimal("100"), -10)).isInstanceOf(IllegalArgumentException.class);
    }
}
