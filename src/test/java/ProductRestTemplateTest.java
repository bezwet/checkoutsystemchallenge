import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kornelkmiecik.checkoutsystem.config.AppConfig;
import com.kornelkmiecik.checkoutsystem.model.Product;
import com.kornelkmiecik.checkoutsystem.services.api.ProductService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import static org.junit.Assert.assertThrows;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = AppConfig.class)
public class ProductRestTemplateTest {

    @Autowired
    private ProductService productService;

    @Autowired
    private RestTemplate restTemplate;

    private MockRestServiceServer mockServer;
    private ObjectMapper mapper = new ObjectMapper();

    final String PRODUCT_THIRD_PARTY_REST_API = "http://localhost:8081/products";

    @Before
    public void init() {
        mockServer = MockRestServiceServer.createServer(restTemplate);
    }

    @Test
    public void canGetSingleProduct() throws JsonProcessingException {
        Product product = new Product("PWWe3w1SDU", "Amazing Burger!", 999);
        mockServer.expect(ExpectedCount.once(),
                requestTo(PRODUCT_THIRD_PARTY_REST_API + "/" + product.getId()))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withStatus(HttpStatus.OK)
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(mapper.writeValueAsString(product))
                );

        Product returnedProduct = productService.getProduct(product.getId());
        Assert.assertEquals(product.getId(), returnedProduct.getId());
        mockServer.verify();
    }

    @Test
    public void throwsExceptionWhenNoProductIsFound() {
        mockServer.expect(ExpectedCount.once(),
                requestTo(PRODUCT_THIRD_PARTY_REST_API + "/AAAAAAAAA"))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withStatus(HttpStatus.NOT_FOUND)
                );
        assertThrows(HttpClientErrorException.class, () -> productService.getProduct("AAAAAAAAA"));
    }

}
