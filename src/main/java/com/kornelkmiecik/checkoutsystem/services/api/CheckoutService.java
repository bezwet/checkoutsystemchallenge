package com.kornelkmiecik.checkoutsystem.services.api;

import com.kornelkmiecik.checkoutsystem.model.Basket;
import com.kornelkmiecik.checkoutsystem.model.Product;
import com.kornelkmiecik.checkoutsystem.model.Receipt;
import com.kornelkmiecik.checkoutsystem.model.promotion.BasePromotion;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

public interface CheckoutService {
    public Receipt checkout(Basket basket);

    public ArrayList<Product> getProductListAfterAllPromotionsApplied(Basket basket);
}
