package com.kornelkmiecik.checkoutsystem.services.api;

import com.kornelkmiecik.checkoutsystem.model.Product;

public interface ProductService {
    Product[] getAllProducts();
    Product getProduct(String id);
}
