package com.kornelkmiecik.checkoutsystem.services.api;

import com.kornelkmiecik.checkoutsystem.model.Product;

import java.util.ArrayList;
import java.util.Collection;

public interface PromotionRequirementService {
    public boolean requirementForPromotionMet(Collection<Product> products);
    ArrayList<Product> getProductsAfterPromotionCheckedAndApplied(ArrayList<Product> productList);
}
