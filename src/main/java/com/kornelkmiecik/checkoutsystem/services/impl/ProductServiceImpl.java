package com.kornelkmiecik.checkoutsystem.services.impl;

import com.kornelkmiecik.checkoutsystem.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.kornelkmiecik.checkoutsystem.services.api.ProductService;
import org.springframework.web.client.RestTemplate;

@Service
public class ProductServiceImpl implements ProductService {
    private RestTemplate restTemplate;

    @Autowired
    public ProductServiceImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    final String PRODUCT_THIRD_PARTY_REST_API = "http://localhost:8081/products";

    public Product[] getAllProducts() {
        ResponseEntity<Product[]> response = restTemplate.getForEntity(PRODUCT_THIRD_PARTY_REST_API, Product[].class);
        return response.getBody();
    }

    public Product getProduct(String id) {
        ResponseEntity<Product> response = restTemplate.getForEntity(PRODUCT_THIRD_PARTY_REST_API + "/" + id, Product.class);
        return response.getBody();
    }
}
