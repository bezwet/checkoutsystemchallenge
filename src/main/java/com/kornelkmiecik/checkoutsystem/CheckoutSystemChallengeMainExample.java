package com.kornelkmiecik.checkoutsystem;

import com.kornelkmiecik.checkoutsystem.model.*;
import com.kornelkmiecik.checkoutsystem.model.promotion.BuyXGetYFreePromotion;
import com.kornelkmiecik.checkoutsystem.model.promotion.FlatPercentPromotion;
import com.kornelkmiecik.checkoutsystem.model.promotion.QtyBasedPromotion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Currency;
import java.util.Locale;

import static java.util.Arrays.asList;

public class CheckoutSystemChallengeMainExample {
    public static void main(String[] args) {

        new Supermarket("Food Shop");

        QtyBasedPromotion qtyBasedPromotion = new QtyBasedPromotion("promotionID1", 1, 400);
        BuyXGetYFreePromotion buyXGetYFreePromotion = new BuyXGetYFreePromotion("promotionID2", 2, 1);
        FlatPercentPromotion flatPercentPromotion = new FlatPercentPromotion("promotionID3", 10);

        Supermarket.promotionsInSupermarket = (asList(qtyBasedPromotion, flatPercentPromotion, buyXGetYFreePromotion));

        User user = new User("Adam", "Smith", new BigDecimal("100"));
        user.setBasket(new Basket());

        Product productA = new Product("productID1", "Pizza", 500, asList(buyXGetYFreePromotion));
        Product productB = new Product("productID2", "Burger", 500, asList(qtyBasedPromotion));
        Product productC = new Product("productID3", "Fries", 1000, asList(flatPercentPromotion));

        user.getBasket().setProductList(new ArrayList<Product>(Arrays.asList(productA, productB, productC)));

        Checkout checkout = new Checkout(Currency.getInstance(Locale.UK));


        Receipt receipt = checkout.checkout(user.getBasket());

        receipt.printTotalToPay();

        if (user.pay(receipt.getTotalPriceToPay())) {
            receipt.printReceipt();
        }


    }


}
