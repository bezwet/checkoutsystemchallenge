package com.kornelkmiecik.checkoutsystem.model;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Objects;

@Getter
@Setter
public class Basket {

    public Basket() {
        this.productList = new ArrayList<>();
    }

    private ArrayList<Product> productList;

    public void addProduct(Product product) {
        this.productList.add(Objects.requireNonNull(product, "Empty Product cannot be added to basket"));
    }

}
