package com.kornelkmiecik.checkoutsystem.model;

import com.kornelkmiecik.checkoutsystem.Utils;
import com.kornelkmiecik.checkoutsystem.model.promotion.BasePromotion;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Product {
    private String id;
    private String name;
    private int price;
    private BigDecimal priceToPay;
    private List<BasePromotion> promotions;

    public Product(String id, String name, int price) {
        this.id = Objects.requireNonNull(id, "ID of Product cannot be null");
        this.name = Objects.requireNonNull(name, "Name of Product cannot be null");
        this.price = price;
        this.priceToPay = Utils.getPenniesToBigDecimalPound(price);
    }

    public Product(String id, String name, int price, List<BasePromotion> promotions) {
        this.id = Objects.requireNonNull(id, "ID of Product cannot be null");
        this.name = Objects.requireNonNull(name, "Name of Product cannot be null");
        this.price = price;
        this.priceToPay = Utils.getPenniesToBigDecimalPound(price);
        this.promotions = promotions;
    }

//        @Override
//        public boolean equals(Object obj) {
//            if (this == obj) {
//                return true;
//            }
//            if (obj == null || getClass() != obj.getClass()) {
//                return false;
//            }
//        }

    public int hashCode() {
        return this.id.hashCode();
    }

    public boolean hasPromotions(){
       return Objects.nonNull(getPromotions()) && !getPromotions().isEmpty();
    }
}
