package com.kornelkmiecik.checkoutsystem.model;

import com.kornelkmiecik.checkoutsystem.model.promotion.BasePromotion;
import com.kornelkmiecik.checkoutsystem.model.promotion.BuyXGetYFreePromotion;
import com.kornelkmiecik.checkoutsystem.model.promotion.FlatPercentPromotion;
import com.kornelkmiecik.checkoutsystem.model.promotion.QtyBasedPromotion;
import com.kornelkmiecik.checkoutsystem.services.api.CheckoutService;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Currency;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
public class Checkout implements CheckoutService {
    private Currency currency;

    public Checkout(Currency currency) {
        this.currency = Objects.requireNonNull(currency);

    }

    @Override
    public Receipt checkout(Basket basket) {
        return new Receipt(this.getProductListAfterAllPromotionsApplied(basket), currency);
    }

    @Override
    public ArrayList<Product> getProductListAfterAllPromotionsApplied(Basket basket) {
        ArrayList<Product> productList = basket.getProductList();
        if (Supermarket.anyPromotionAvailable()) {
            for (BasePromotion basePromotion : Supermarket.promotionsInSupermarket) {
                switch (basePromotion.getType()) {
                    case QTY_BASED_PRICE_OVERRIDE:
                        productList = this.getProductsAfterQtyBasedPricePromotionApplied((QtyBasedPromotion) basePromotion, productList);
                        break;
                    case BUY_X_GET_Y_FREE:
                        productList = this.getProductsAfterBuyXGetYPromotionApplied((BuyXGetYFreePromotion) basePromotion, productList);
                        break;
                    case FLAT_PERCENT:
                        productList = this.getProductsAfterFlatPercentagePromotionApplied((FlatPercentPromotion) basePromotion, productList);
                        break;
                    default:
                        break;
                }
            }
        }
        return productList;
    }

    public ArrayList<Product> getProductsAfterQtyBasedPricePromotionApplied(QtyBasedPromotion qtyBasedPromotion, ArrayList<Product> productList) {
        return qtyBasedPromotion.getProductsAfterPromotionCheckedAndApplied(productList);
    }

    public ArrayList<Product> getProductsAfterBuyXGetYPromotionApplied(BuyXGetYFreePromotion buyXGetYFreePromotion, ArrayList<Product> productList) {
        return buyXGetYFreePromotion.getProductsAfterPromotionCheckedAndApplied(productList);
    }

    public ArrayList<Product> getProductsAfterFlatPercentagePromotionApplied(FlatPercentPromotion flatPercentPromotion, ArrayList<Product> productList) {
        return flatPercentPromotion.getProductsAfterPromotionApplied(productList);
    }

}
