package com.kornelkmiecik.checkoutsystem.model;

import com.kornelkmiecik.checkoutsystem.Utils;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Objects;

@Getter
@Setter
public class User {

    public User(String firstname, String lastname, BigDecimal walletAmountInPounds) {
        this.firstname = Objects.requireNonNull(firstname, "firstname of User cannot be null");
        this.lastname = Objects.requireNonNull(lastname, "lastname of User cannot be null");
        this.walletAmountInPounds = Objects.requireNonNull(walletAmountInPounds, "walletAmountInPounds of User cannot be null");
    }

    private String firstname;
    private String lastname;
    private BigDecimal walletAmountInPounds;
    Basket basket;

    public boolean pay(BigDecimal totalToPay) {
        checkTotalToPayForCorrectness(totalToPay);
        if (Utils.firstDecimalSmallerThanSecond(this.walletAmountInPounds, totalToPay)) {
            System.out.println("Not enough funds to pay");
            return false;
        } else {
            this.walletAmountInPounds = this.walletAmountInPounds.subtract(totalToPay);
            System.out.println("Thank You for payment");
            return true;
        }
    }

    public void checkTotalToPayForCorrectness(BigDecimal totalToPay) {
        Objects.requireNonNull(totalToPay, "totalToPay cannot be null");
        if (Utils.firstDecimalSmallerThanSecond(totalToPay, new BigDecimal("0"))) {
            throw new IllegalArgumentException("Total price to pay cannot be less than 0");
        }
    }
}
