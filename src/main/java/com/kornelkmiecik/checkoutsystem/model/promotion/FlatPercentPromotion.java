package com.kornelkmiecik.checkoutsystem.model.promotion;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.kornelkmiecik.checkoutsystem.Utils;
import com.kornelkmiecik.checkoutsystem.model.Product;
import com.kornelkmiecik.checkoutsystem.model.enums.PromotionType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class FlatPercentPromotion extends BasePromotion {
    private int amount;

    public FlatPercentPromotion(String id, int amount) {
        super(id, PromotionType.FLAT_PERCENT);
        this.amount = amount;
        if (amount < 0) {
            throw new IllegalArgumentException("amount cannot be less than 0");
        }
    }


    public ArrayList<Product> getProductsAfterPromotionApplied(ArrayList<Product> products) {
        for(Product product: products) {
            if (product.hasPromotions()) {
                    if (productHasThisPromotion(product)) {
                        product.setPriceToPay(Utils.getBigDecimalReducedByGivenIntPercent(product.getPriceToPay(), amount));
                    }
                }
        }
        return products;
    }

}
