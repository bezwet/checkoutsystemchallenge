package com.kornelkmiecik.checkoutsystem.model.promotion;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.kornelkmiecik.checkoutsystem.Utils;
import com.kornelkmiecik.checkoutsystem.model.Product;
import com.kornelkmiecik.checkoutsystem.model.enums.PromotionType;
import com.kornelkmiecik.checkoutsystem.services.api.PromotionRequirementService;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Collection;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class QtyBasedPromotion extends BasePromotion implements PromotionRequirementService {
    private int required_qty;
    private int price;

    public QtyBasedPromotion(String id, int required_qty, int price) {
        super(id, PromotionType.QTY_BASED_PRICE_OVERRIDE);
        this.required_qty = required_qty;
        this.price = price;
        if (required_qty < 0) {
            throw new IllegalArgumentException("required_qty cannot be less than 0");
        }
        if (price < 0) {
            throw new IllegalArgumentException("price cannot be less than 0");
        }

    }

    @Override
    public boolean requirementForPromotionMet(Collection<Product> products) {
        return countProductsThatSharePromotion(products) >= this.required_qty;
    }


    public ArrayList<Product> getProductsAfterPromotionApplied(ArrayList<Product> products) {
        for (Product product : products) {
            if (product.hasPromotions()) {
                if (productHasThisPromotion(product)) {
                    product.setPriceToPay(Utils.getPenniesToBigDecimalPound(this.price));
                }
            }
        }
        return products;
    }

    @Override
    public ArrayList<Product> getProductsAfterPromotionCheckedAndApplied(ArrayList<Product> productList) {
        if (requirementForPromotionMet(productList)) {
            return getProductsAfterPromotionApplied(productList);
        } else
            return productList;
    }
}
