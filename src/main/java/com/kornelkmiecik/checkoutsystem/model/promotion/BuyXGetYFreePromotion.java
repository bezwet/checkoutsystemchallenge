package com.kornelkmiecik.checkoutsystem.model.promotion;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.kornelkmiecik.checkoutsystem.model.Product;
import com.kornelkmiecik.checkoutsystem.model.enums.PromotionType;
import com.kornelkmiecik.checkoutsystem.services.api.PromotionRequirementService;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class BuyXGetYFreePromotion extends BasePromotion implements PromotionRequirementService {

    public BuyXGetYFreePromotion(String id, int required_qty, int free_qty) {
        super(id, PromotionType.BUY_X_GET_Y_FREE);
        this.required_qty = Objects.requireNonNull(required_qty, "required_qty cannot be null");
        this.free_qty = Objects.requireNonNull(free_qty, "free_qty cannot be null");
        if (free_qty < 0) {
            throw new IllegalArgumentException("free_qty cannot be less than 0");
        }
        if (required_qty < 0) {
            throw new IllegalArgumentException("required_qty cannot be less than 0");
        }
        if (required_qty <= free_qty) {
            throw new IllegalArgumentException("required_qty should not be less than free_qty. Otherwise all products with this promotion are free");
        }
    }

    private int required_qty;
    private int free_qty;

    @Override
    public boolean requirementForPromotionMet(Collection<Product> products) {
        return countProductsThatSharePromotion(products) >= this.required_qty;
    }

    public ArrayList<Product> getProductsAfterPromotionApplied(ArrayList<Product> products) {
        int promotionsAppliedTimes = 0;
        for (Product product : products) {
            if (product.hasPromotions() && promotionsAppliedTimes < free_qty) {
                if (productHasThisPromotion(product)) {
                    product.setPriceToPay(BigDecimal.valueOf(0));
                    promotionsAppliedTimes++;
                }
            }
        }
        return products;
    }

    @Override
    public ArrayList<Product> getProductsAfterPromotionCheckedAndApplied(ArrayList<Product> productList) {
        if (requirementForPromotionMet(productList)) {
            return getProductsAfterPromotionApplied(productList);
        } else
            return productList;
    }

}
