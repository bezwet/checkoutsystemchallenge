package com.kornelkmiecik.checkoutsystem.model.promotion;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.kornelkmiecik.checkoutsystem.model.Product;
import com.kornelkmiecik.checkoutsystem.model.enums.PromotionType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Collection;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class BasePromotion {
    private String id;
    private PromotionType type;

    public BasePromotion(String id, PromotionType type) {
        this.id = Objects.requireNonNull(id, "ID of Promotion cannot be null");
        this.type = Objects.requireNonNull(type, "PromotionType of Promotion cannot be null");
    }

    public boolean equals(Object object) {
        if (object instanceof BasePromotion) {
            BasePromotion otherBaseEntity = (BasePromotion) object;
            return this.id.equals(otherBaseEntity.id);
        } else {
            return false;
        }
    }

    public int countProductsThatSharePromotion(Collection<Product> products) {
        int productsThatAppliesForPromotion = 0;
        for (Product product : products) {
            if (Objects.nonNull(product.getPromotions()) && !product.getPromotions().isEmpty()) {
                for (int i = 0; i < product.getPromotions().size(); i++) {
                    if (product.getPromotions().get(i).id.equals(this.id)) {
                        productsThatAppliesForPromotion++;
                    }
                }
            }
        }
        return productsThatAppliesForPromotion;
    }

    public boolean productHasThisPromotion(Product product) {
        for (int i = 0; i < product.getPromotions().size(); i++) {
            if (product.getPromotions().get(i).getId().equals(this.getId())) {
                return true;
            }
        }
        return false;
    }

    public int hashCode() {
        return this.id.hashCode();
    }


}
