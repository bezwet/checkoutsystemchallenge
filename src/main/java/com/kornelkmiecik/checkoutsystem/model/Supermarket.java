package com.kornelkmiecik.checkoutsystem.model;

import com.kornelkmiecik.checkoutsystem.model.promotion.BasePromotion;

import java.util.Collection;
import java.util.Objects;

public class Supermarket {

    public Supermarket(String supermarketName) {
        this.supermarketName = Objects.requireNonNull(supermarketName);
    }

    public Supermarket(String supermarketName, Collection<BasePromotion> promotionsInSupermarket) {
        this.supermarketName = Objects.requireNonNull(supermarketName);
        this.promotionsInSupermarket = Objects.requireNonNull(promotionsInSupermarket);
    }
    static public Collection<BasePromotion> promotionsInSupermarket;
    static public String supermarketName;

    static public boolean anyPromotionAvailable() {
        return promotionsInSupermarket != null && !promotionsInSupermarket.isEmpty();
    }
}
