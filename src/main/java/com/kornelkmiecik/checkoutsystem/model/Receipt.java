package com.kornelkmiecik.checkoutsystem.model;

import com.kornelkmiecik.checkoutsystem.Utils;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Currency;
import java.util.Locale;
import java.util.Objects;

@Getter
@Setter
public class Receipt {
    private ArrayList<Product> productList;
    private LocalDateTime receiptDate;
    private Currency currency;

    public Receipt(ArrayList<Product> productList, Currency currency) {
        this.productList = Objects.requireNonNull(productList, "Receipt product list cannot be null");
        this.receiptDate = LocalDateTime.now();
        this.currency = Objects.requireNonNull(currency, "Currency cannot be null");;
    }

    public BigDecimal getTotalPriceToPay() {
        return productList.stream()
                .map(product -> product.getPriceToPay())
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }


    public BigDecimal getTotalBasePriceInBigDecimals() {
        return Utils.getPenniesToBigDecimalPound(productList.stream().mapToInt(product -> product.getPrice()).reduce(0, (x, y) -> x + y));
    }

    public BigDecimal getTotalDeductions() {
        BigDecimal totalBasePriceInBigDecimals = getTotalBasePriceInBigDecimals();
        BigDecimal totalPriceToPay = getTotalPriceToPay();
        if (Utils.firstDecimalSmallerThanSecond(totalBasePriceInBigDecimals, totalPriceToPay)) {
            throw new IllegalArgumentException("Price after discounts(totalPriceToPay) should not be higher than without(totalBasePriceInBigDecimals)");
        }
        return totalBasePriceInBigDecimals.subtract(totalPriceToPay);
    }

    public void printReceipt() {
        printShopName();
        printReceiptDate();
        printProductsList();
        printBasePriceTotal();
        printTotalToPay();
        printTotalDeductions();
    }

    public void printReceiptDate() {
        System.out.println(receiptDate.format(Utils.dateTimeFormatter));
    }

    public void printShopName() {
        System.out.println(Supermarket.supermarketName);
    }

    public void printProductsList() {
        productList.forEach(product -> {
            System.out.println(product.getName() + "  " + product.getPriceToPay() + " " + this.currency);
        });
    }

    public void printBasePriceTotal() {
        System.out.println("TOTAL BASE PRICE: " + getTotalBasePriceInBigDecimals() + " " + this.currency);
    }

    public void printTotalToPay() {
        System.out.println("TOTAL PRICE TO PAY: " + getTotalPriceToPay() + " " + this.currency);
    }

    public void printTotalDeductions() {
        System.out.println("TOTAL DEDUCTIONS: " + getTotalDeductions() + " " + this.currency);
    }


}
