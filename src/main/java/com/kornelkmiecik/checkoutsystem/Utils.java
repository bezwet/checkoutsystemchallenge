package com.kornelkmiecik.checkoutsystem;

import java.math.BigDecimal;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

public class Utils {
    static BigDecimal penniesInPound = new BigDecimal(100);
    public static DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT);

    static public BigDecimal getPenniesToBigDecimalPound(int pennies) {
        return BigDecimal.valueOf(pennies).divide(penniesInPound);
    }

    static public BigDecimal getBigDecimalReducedByGivenIntPercent(BigDecimal price, int percent) {
        return price.subtract(getPercentOutOfDecimal(price, percent));
    }

    static public BigDecimal getPercentOutOfDecimal(BigDecimal price, int percent) {
        if (percent < 0) {
            throw new IllegalArgumentException("Percent cannot be less than 0");
        }
        if (percent == 0) {
            return new BigDecimal("0");
        }
        return price.multiply(BigDecimal.valueOf(percent).divide(new BigDecimal("100")));
    }

    static public Boolean firstDecimalSmallerThanSecond(BigDecimal firstDecimal, BigDecimal secondDecimal) {
        return firstDecimal.compareTo(secondDecimal) < 0;
    }
}
